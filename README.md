## Install
```go get -u bitbucket.org/nndialer/configuration```

## What is Configuration?

Configuration is a golang package which helps to collect configuration variables for your app. It will take the data from environment variables and/or from formatted TOML configuration files. Configuration supports nested structs by using recursion.

## Define your struct:

Each field of the struct should have atleast one tag for environment and/or toml sources.

For environment vars tag is: `env`
For toml files: `toml`

If both tags are specified and both sources presents, then priority will go to environment variables.

### Example:

```go
type Config struct {
    P1 string `env:"TEST_P1" toml:"p1"`
	P2 int64  `env:"TEST_P2" toml:"p2"`
    P3 uint   `env:"TEST_P3" toml:"p3"`
    P4 bool   `env:"TEST_P4" toml:"p4"`
}
```

## Example:

```go
package main

import (
	"configuration"
	"flag"
	"fmt"

	"github.com/golang/glog"
)

// Struct1 is the main struct with configuration data
type Struct1 struct {
	Field1 string  `toml:"field_1" env:"FIELD_1"`
	Field2 uint64  `toml:"field_2" env:"FIELD_2"`
	Field3 Struct2 `toml:"field_3" env:"FIELD_3"`
}

// Struct2 is a nested struct
type Struct2 struct {
	Field4 uint64 `toml:"field_4" env:"FIELD_4"`
	Field5 int32  `toml:"field_5" env:"FIELD_5"`
}

func main() {
	cFilePath := flag.String("c", "my.conf", "path to configuration file")
	flag.Parse()

	conf := Struct1{}
	err := configuration.GetConfig(&conf, *cFilePath)
	if err != nil {
		glog.Error(err)
		return
	}
	fmt.Printf("%+v", conf)
}

```

