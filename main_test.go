package configuration

import (
	"os"
	"testing"
)

func TestGetConfig(t *testing.T) {
	type Args struct {
		S     interface{}
		cPath string
	}
	tests := []struct {
		name    string
		Args    Args
		wantErr bool
		f       func()
	}{
		{
			name: "Simple struct",
			Args: Args{
				S: &struct {
					P1 string `env:"TEST_P1" toml:"p1"`
					P2 int64  `env:"TEST_P2" toml:"p2"`
					P3 uint   `env:"TEST_P3" toml:"p3"`
					P4 bool   `env:"TEST_P4" toml:"p4"`
				}{},
				cPath: "tests/conf-files/t1.conf",
			},
			wantErr: false,
			f: func() {
				err := os.Setenv("TEST_P1", "tratatta")
				err = os.Setenv("TEST_P2", "-567")
				err = os.Setenv("TEST_P3", "345")
				err = os.Setenv("TEST_P4", "false")
				if err != nil {
					t.Error(err)
				}
			},
		},
		{
			name: "Inherited struct",
			Args: Args{
				S: &struct {
					P1 string `env:"TEST_P1" toml:"p1"`
					P2 int64  `env:"TEST_P2" toml:"p2"`
					P3 uint   `env:"TEST_P3" toml:"p3"`
					P4 bool   `env:"TEST_P4" toml:"p4"`
					P5 struct {
						D1 string `env:"TEST_D1" toml:"d1"`
						D2 int64  `env:"TEST_D2" toml:"d2"`
					} `env:"pofig" toml:"new"`
				}{},
				cPath: "tests/conf-files/t2.conf",
			},
			wantErr: false,
			f: func() {
				err := os.Setenv("TEST_P1", "tratatta")
				err = os.Setenv("TEST_P2", "-567")
				err = os.Setenv("TEST_P4", "true")
				err = os.Setenv("TEST_D1", "dqwfqwfq")
				if err != nil {
					t.Error(err)
				}
			},
		},
		{
			name: "Wrong configuration file",
			Args: Args{
				S: &struct {
					P1 string `env:"TEST_P1" toml:"p1"`
					P2 int64  `env:"TEST_P2" toml:"p2"`
					P3 uint   `env:"TEST_P3" toml:"p3"`
					P4 bool   `env:"TEST_P4" toml:"p4"`
					P5 struct {
						D1 string `env:"TEST_D1" toml:"d1"`
						D2 int64  `env:"TEST_D2" toml:"d2"`
					} `env:"pofig" toml:"new"`
				}{},
				cPath: "tests/conf-files/t234.conf",
			},
			wantErr: true,
		},
		{
			name: "Wrong type for int64",
			Args: Args{
				S: &struct {
					P1 string `env:"TEST_P1" toml:"p1"`
					P2 int64  `env:"TEST_P2" toml:"p2"`
					P3 uint   `env:"TEST_P3" toml:"p3"`
					P4 bool   `env:"TEST_P4" toml:"p4"`
					P5 struct {
						D1 string `env:"TEST_D1" toml:"d1"`
						D2 int64  `env:"TEST_D2" toml:"d2"`
					} `env:"pofig" toml:"new"`
				}{},
				cPath: "tests/conf-files/t234.conf",
			},
			wantErr: true,
			f: func() {
				err := os.Setenv("TEST_P2", "fwefwe")
				if err != nil {
					t.Error(err)
				}
			},
		},
		{
			name: "Wrong type for bool",
			Args: Args{
				S: &struct {
					P1 string `env:"TEST_P1" toml:"p1"`
					P2 int64  `env:"TEST_P2" toml:"p2"`
					P3 uint   `env:"TEST_P3" toml:"p3"`
					P4 bool   `env:"TEST_P4" toml:"p4"`
					P5 struct {
						D1 string `env:"TEST_D1" toml:"d1"`
						D2 int64  `env:"TEST_D2" toml:"d2"`
					} `env:"pofig" toml:"new"`
				}{},
			},
			wantErr: true,
			f: func() {
				err := os.Setenv("TEST_P4", "fwefwe")
				if err != nil {
					t.Error(err)
				}
			},
		},
		{
			name: "Wrong type for uint",
			Args: Args{
				S: &struct {
					P1 string `env:"TEST_P1" toml:"p1"`
					P2 int64  `env:"TEST_P2" toml:"p2"`
					P3 uint   `env:"TEST_P3" toml:"p3"`
					P4 bool   `env:"TEST_P4" toml:"p4"`
					P5 struct {
						D1 string `env:"TEST_D1" toml:"d1"`
						D2 int64  `env:"TEST_D2" toml:"d2"`
					} `env:"pofig" toml:"new"`
				}{},
			},
			wantErr: true,
			f: func() {
				err := os.Setenv("TEST_P3", "-1")
				if err != nil {
					t.Error(err)
				}
			},
		},
		{
			name: "No variables were specified",
			Args: Args{
				S: &struct {
					S2 struct {
						P4 bool `env:"TEST_P5" toml:"p4"`
					}
					P1 string `env:"TEST_P1" toml:"p1"`
					P2 int64  `env:"TEST_P2" toml:"p2"`
					P3 uint   `env:"TEST_P3" toml:"p3"`
					P4 bool   `env:"TEST_P4" toml:"p4"`
					P5 struct {
						D1 string `env:"TEST_D1" toml:"d1"`
						D2 int64  `env:"TEST_D2" toml:"d2"`
					} `env:"pofig" toml:"new"`
				}{},
			},
			wantErr: true,
			f: func() {
				err := os.Setenv("TEST_P5", "true")
				err = os.Setenv("TEST_P4", "true")
				if err != nil {
					t.Error(err)
				}
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.f != nil {
				tt.f()
			}
			err := GetConfig(tt.Args.S, tt.Args.cPath)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetConfig() error = %v, wantErr %v", err, tt.wantErr)
			} else {
				t.Log(err)
			}
			t.Logf("%+v", tt.Args.S)
			os.Clearenv()
		})
	}
}
