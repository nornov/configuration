package configuration

import (
	"fmt"
	"os"
	"reflect"
	"strconv"

	"github.com/BurntSushi/toml"
)

func getEnvConfig(s interface{}) error {
	var cRefl reflect.Value
	if t := reflect.TypeOf(s); t.Kind() == reflect.Ptr {
		cRefl = reflect.ValueOf(s).Elem()
	} else {
		cRefl = reflect.ValueOf(s)
	}
	for i := 0; i < cRefl.NumField(); i++ {
		v := cRefl.Field(i)
		tv := os.Getenv(cRefl.Type().Field(i).Tag.Get("env"))
		switch v.Kind() {
		case reflect.Struct:
			nv := reflect.New(v.Type())
			err := getEnvConfig(nv.Interface())
			if err != nil {
				return err
			}
			v.Set(nv.Elem())
		case reflect.String:
			v.SetString(tv)
		case reflect.Bool:
			if tv == "true" {
				v.SetBool(true)
			} else {
				v.SetBool(false)
			}
			// } else if tv != "false" {
			// 	return fmt.Errorf("Value for %s should be true or false", cRefl.Type().Field(i).Name)
			// }
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			if tv != "" {
				iv, err := strconv.ParseInt(tv, 10, 64)
				if err != nil {
					return fmt.Errorf("Can't convert %s to integer for field %s", tv, cRefl.Type().Field(i).Name)
				}
				v.SetInt(iv)
			}
		case reflect.Uint, reflect.Uint16, reflect.Uint32, reflect.Uint64:
			if tv != "" {
				uv, err := strconv.ParseUint(tv, 10, 64)
				if err != nil {
					return fmt.Errorf("Can't convert %s to unsigned integer for field %s", tv, cRefl.Type().Field(i).Name)
				}
				v.SetUint(uv)
			}
		case reflect.Float32, reflect.Float64:
			if tv != "" {
				uv, err := strconv.ParseFloat(tv, 64)
				if err != nil {
					return fmt.Errorf("Can't convert %s to unsigned integer for field %s", tv, cRefl.Type().Field(i).Name)
				}
				v.SetFloat(uv)
			}
		}
	}
	return nil
}

func getFileConfig(s interface{}, cPath string) error {
	_, err := toml.DecodeFile(cPath, s)
	if err != nil {
		return err
	}
	return nil
}

func checkEFStructs(e, f reflect.Value) error {
	for i := 0; i < e.NumField(); i++ {
		ev := e.Field(i)
		fv := reflect.Indirect(f).Field(i)
		if ev.Kind() == reflect.Struct {
			err := checkEFStructs(ev, fv)
			if err != nil {
				return nil
			}
		}
		eVar := os.Getenv(e.Type().Field(i).Tag.Get("env"))
		if eVar == "-" {
			ev.Set(reflect.Zero(reflect.TypeOf(ev.Interface())))
			continue
		}
		if reflect.DeepEqual(ev.Interface(), reflect.Zero(reflect.TypeOf(ev.Interface())).Interface()) {
			if ev.Kind() == reflect.Bool && eVar != "" {
				continue
			}
			if reflect.DeepEqual(fv.Interface(), reflect.Zero(reflect.TypeOf(fv.Interface())).Interface()) && reflect.TypeOf(fv.Interface()).Kind() != reflect.Bool {
				return fmt.Errorf("Configuration variable %s isn't set", e.Type().Field(i).Name)
			}
			ev.Set(fv)
		}
	}
	return nil
}

// GetConfig check configuration variables for specified struct
func GetConfig(s interface{}, cPath string) error {
	err := getEnvConfig(s)
	if err != nil {
		return err
	}
	ec := reflect.ValueOf(s).Elem()
	fc := reflect.New(ec.Type())
	if _, err = os.Stat(cPath); err != nil && cPath != "" {
		return err
	} else if err == nil {
		err = getFileConfig(fc.Interface(), cPath)
		if err != nil {
			return err
		}
	}
	return checkEFStructs(ec, fc)
}
